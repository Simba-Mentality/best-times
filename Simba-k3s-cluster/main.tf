terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 3.59.0"
    }
    #kubernetes = {
    #  source  = "hashicorp/kubernetes"
    #  version = "<= 2.0.0"
    #}
    #rancher2 = {
    #  source  = "rancher/rancher2"
    #  version = ">= 1.10.0"
    #}
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.0"
    }

  }
}

provider "aws" {
  region     = "us-east-1"
}

resource "aws_instance" "master" {
  depends_on = [module.ssh_key_pair]
  ami        = "ami-0aa2b7722dc1b5612" # ubuntu image.
  # cluster-asg role for server node
  #iam_instance_profile = "ServerNode-Role"
  #key_name = "my-key"
  key_name               = module.ssh_key_pair.key_name
  vpc_security_group_ids = [aws_security_group.k3s_server.id]
  instance_type          = "t3a.medium" #nioyatech hesabında t3.medium instance olmadığı için t3a.medium ile değiştirdim
  user_data = base64encode(templatefile("${path.module}/server-userdata.tmpl", {
    token = random_password.k3s_cluster_secret.result
  })) # token = shared secret used to join a server or agent to a cluster

  tags = {
    Name = "k3sServer"
    #env  = "dev"
  }
}

resource "aws_instance" "worker" {
  #depends_on           = [module.ssh_key_pair]
  ami = "ami-0aa2b7722dc1b5612" # ubuntu image
  #iam_instance_profile = "AgentNode-Role"
  #count    = 2
  #key_name = "my-key"
  key_name               = module.ssh_key_pair.key_name
  vpc_security_group_ids = [aws_security_group.k3s_agent.id]
  #placement_group        = aws_placement_group.k3s.id
  instance_type = "t3a.medium" # 2 vCPU and 1 GIB Memory, nioyatech hesabında t3.medium instance olmadığı için t3a.medium ile değiştirdim
  user_data = base64encode(templatefile("${path.module}/agent-userdata.tmpl", {
    host  = aws_instance.master.private_ip,
    token = random_password.k3s_cluster_secret.result
  }))
  tags = {
    Name = "k3sWorker"
  }
}

# to create key pem for instances 
module "ssh_key_pair" {
  source = "cloudposse/key-pair/aws"
  ## Cloud Posse recommends pinning every module to a specific version
  version               = "0.18.3" # local'ime yüklü terraformun istediği #version="0.18.3"
  name                  = "project_x"
  ssh_public_key_path   = "./pem_key"
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
}

